package com.example.test.ekassir.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by a.grigal on 06.02.2017.
 */

public class CacheStore {
    private static final String TAG = "CacheStore";
    private static CacheStore INSTANCE = null;
    private HashMap<String, Pair<Long,String>> cacheMap;
    private HashMap<String, Pair<Long,Bitmap>> bitmapMap;
    private static final String cacheDir = "/Android/data/com.test1/cache/";
    private static final String CACHE_FILENAME = ".cache";

    @SuppressWarnings("unchecked")
    private CacheStore() {
        cacheMap = new HashMap<String, Pair<Long,String>>();
        bitmapMap = new HashMap<String, Pair<Long,Bitmap>>();
        File fullCacheDir = new File(Environment.getExternalStorageDirectory().toString(),cacheDir);
        if(!fullCacheDir.exists()) {
            Log.i(TAG, "Directory doesn't exist");
            cleanCacheStart();
            return;
        }
        try {
            ObjectInputStream is = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(fullCacheDir.toString(), CACHE_FILENAME))));
            cacheMap = (HashMap<String,Pair<Long,String>>)is.readObject();
            is.close();
        } catch (StreamCorruptedException e) {
            Log.i(TAG, "Corrupted stream");
            cleanCacheStart();
        } catch (FileNotFoundException e) {
            Log.i(TAG, "File not found");
            cleanCacheStart();
        } catch (IOException e) {
            Log.i(TAG, "Input/Output error");
            cleanCacheStart();
        } catch (ClassNotFoundException e) {
            Log.i(TAG, "Class not found");
            cleanCacheStart();
        }
    }

    private void cleanCacheStart() {
        cacheMap = new HashMap<String,Pair<Long,String>>();
        File fullCacheDir = new File(Environment.getExternalStorageDirectory().toString(),cacheDir);
        fullCacheDir.mkdirs();
        File noMedia = new File(fullCacheDir.toString(), ".nomedia");
        try {
            noMedia.createNewFile();
            Log.i(TAG, "Cache created");
        } catch (IOException e) {
            Log.i(TAG, "Couldn't create .nomedia file");
            e.printStackTrace();
        }
    }

    private synchronized static void createInstance() {
        if(INSTANCE == null) {
            INSTANCE = new CacheStore();
        }
    }

    public static CacheStore getInstance() {
        if(INSTANCE == null) createInstance();
        return INSTANCE;
    }

    public void saveCacheFile(String cacheUri, Bitmap image) {
        File fullCacheDir = new File(Environment.getExternalStorageDirectory().toString(),cacheDir);
        String fileLocalName = new SimpleDateFormat("ddMMyyhhmmssSSS").format(new java.util.Date())+".JPG";
        File fileUri = new File(fullCacheDir.toString(), fileLocalName);
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(fileUri);
            image.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
            cacheMap.put(cacheUri, Pair.create(getTimeAddTen(), fileLocalName));
            Log.i(TAG, "Saved file "+cacheUri+" (which is now "+fileUri.toString()+") correctly");
            bitmapMap.put(cacheUri, Pair.create(getTimeAddTen(), image));
            ObjectOutputStream os = new ObjectOutputStream(new BufferedOutputStream(
                    new FileOutputStream(new File(fullCacheDir.toString(), CACHE_FILENAME))));
            os.writeObject(cacheMap);
            os.close();
        } catch (FileNotFoundException e) {
            Log.i(TAG, "Error: File "+cacheUri+" was not found!");
            e.printStackTrace();
        } catch (IOException e) {
            Log.i(TAG, "Error: File could not be stuffed!");
            e.printStackTrace();
        }
    }

    @Nullable
    public Bitmap getCacheFile(String cacheUri) {
        if(bitmapMap.containsKey(cacheUri)) {
            if (System.currentTimeMillis() < bitmapMap.get(cacheUri).first) {
                return (Bitmap) bitmapMap.get(cacheUri).second;
            } else {
                if (cacheMap.containsKey(cacheUri)) cacheMap.remove(cacheUri);
                bitmapMap.remove(cacheUri);
                return null;
            }
        }

        if(!cacheMap.containsKey(cacheUri)) return null;

        String fileLocalName = cacheMap.get(cacheUri).second.toString();
        File fullCacheDir = new File(Environment.getExternalStorageDirectory().toString(), cacheDir);
        File fileUri = new File(fullCacheDir.toString(), fileLocalName);

        if (System.currentTimeMillis() < cacheMap.get(cacheUri).first) {
            if (!fileUri.exists()) return null;

            Log.i(TAG, "File " + cacheUri + " has been found in the Cache");
            Bitmap bm = BitmapFactory.decodeFile(fileUri.toString());
            bitmapMap.put(cacheUri, Pair.create(getTimeAddTen(), bm));

            return bm;
        } else {
            Log.i(TAG, "File " + cacheUri + " has been found and deleted in the Cache");
            fileUri.delete();
            cacheMap.remove(cacheUri);

            return null;
        }
    }

    private Long getTimeAddTen() {
        Calendar d = Calendar.getInstance();
        d.add(Calendar.MINUTE, 1);
        return d.getTimeInMillis();
    }
}