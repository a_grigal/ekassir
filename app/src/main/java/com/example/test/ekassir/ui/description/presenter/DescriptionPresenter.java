package com.example.test.ekassir.ui.description.presenter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.example.test.ekassir.data.RestService;
import com.example.test.ekassir.ui.ActivityContract;
import com.example.test.ekassir.util.CacheStore;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by a.grigal on 07.02.2017.
 */

public class DescriptionPresenter implements ActivityContract.DescriptionPresenter {

    public Retrofit retrofit;
    ActivityContract.View mView;

    @Inject
    public DescriptionPresenter(Retrofit retrofit, ActivityContract.View mView) {
        this.retrofit = retrofit;
        this.mView = mView;
    }

    @Override
    public void loadPhoto(final String photoId) {
        Bitmap bm = CacheStore.getInstance().getCacheFile(photoId);
        if (bm == null) {
            retrofit.create(RestService.class).getPhoto(photoId).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .unsubscribeOn(Schedulers.io())
                    .subscribe(new Observer<ResponseBody>() {
                        @Override
                        public void onCompleted() {
                            mView.showComplete();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mView.showError(e.getMessage());
                        }

                        @Override
                        public void onNext(ResponseBody response) {
                            Log.d("", "onNext: ");
                            Bitmap bm = BitmapFactory.decodeStream(response.byteStream());
                            CacheStore.getInstance().saveCacheFile(photoId, bm);
                            mView.showPhoto(bm);
                        }
                    });
        } else {
            mView.showPhoto(bm);
        }
    }

}

