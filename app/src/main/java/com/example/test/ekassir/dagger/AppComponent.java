package com.example.test.ekassir.dagger;

import com.example.test.ekassir.ui.description.view.DescriptionActivity;
import com.example.test.ekassir.ui.main.view.MainActivity;

import dagger.Component;

/**
 * Created by Alexander Grigal on 05.02.2017
 */
@CustomScope
@Component(dependencies = NetComponent.class, modules = ActivityModule.class)
public interface AppComponent {
    void inject(MainActivity activity);
    void inject(DescriptionActivity activity);
}
