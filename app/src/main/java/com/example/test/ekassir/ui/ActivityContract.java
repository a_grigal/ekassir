package com.example.test.ekassir.ui;

import android.content.Context;
import android.graphics.Bitmap;

import com.example.test.ekassir.data.model.Order;

import java.util.List;

/**
 * Created by Alexander Grigal on 05.02.2017
 */
public interface ActivityContract {
    interface View {
        void showOrders(List<Order> orders);
        void showPhoto(Bitmap bitmap);
        void showError(String message);
        void showComplete();
    }

    interface ItemClickListener {
        void onClick(Order order);    }

    interface MainPresenter {
        void loadOrders();
        void openDescription(Context ctx, Order order);
    }

    interface DescriptionPresenter {
        void loadPhoto(String photoId);
    }
}
