package com.example.test.ekassir;

import android.app.Application;

import com.example.test.ekassir.dagger.DaggerNetComponent;
import com.example.test.ekassir.dagger.NetComponent;
import com.example.test.ekassir.dagger.AppModule;
import com.example.test.ekassir.dagger.NetModule;

/**
 * Created by Alexander Grigal on 05.02.2017
 */
public class EkassirApp extends Application {
    private NetComponent mNetComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("http://careers.ekassir.com/"))
                .build();
    }

    public NetComponent getNetComponent() {
        return mNetComponent;
    }
}

