package com.example.test.ekassir.data.model;

import com.example.test.ekassir.util.DateHelper;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Alexander Grigal on 05.02.2017
 */


public class Order implements Comparable, Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("startAddress")
    @Expose
    private StartAddress startAddress;
    @SerializedName("endAddress")
    @Expose
    private EndAddress endAddress;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("orderTime")
    @Expose
    private String orderTime;
    @SerializedName("vehicle")
    @Expose
    private Vehicle vehicle;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StartAddress getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(StartAddress startAddress) {
        this.startAddress = startAddress;
    }

    public EndAddress getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(EndAddress endAddress) {
        this.endAddress = endAddress;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public int compareTo(Object obj) {
        Order order = (Order) obj;
        long curObjEpoch = DateHelper.getMillisecondsSinceEpoch(this.getOrderTime());
        long otherObjEpoch = DateHelper.getMillisecondsSinceEpoch(order.getOrderTime());

        if (curObjEpoch < otherObjEpoch) {
            return 1;
        } else if (curObjEpoch > otherObjEpoch) {
            return -1;
        }
        return 0;
    }

}

