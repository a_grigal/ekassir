package com.example.test.ekassir.dagger;

import com.example.test.ekassir.ui.ActivityContract;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Alexander Grigal on 05.02.2017
 */
@Module
public class ActivityModule {
    private final ActivityContract.View mView;


    public ActivityModule(ActivityContract.View view) {
        this.mView = view;
    }

    @Provides
    @CustomScope
    ActivityContract.View providesMainScreenContractView() {
        return mView;
    }

}
