package com.example.test.ekassir.data;

import com.example.test.ekassir.data.model.Order;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by a.grigal on 07.02.2017.
 */

public interface RestService {
    @GET("/test/orders.json")
    Observable<List<Order>> getOrderList();

    @GET("/test/images/{imageId}")
    Observable<ResponseBody> getPhoto(@Path("imageId") String taskId);
}
