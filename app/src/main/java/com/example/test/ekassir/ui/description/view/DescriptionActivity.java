package com.example.test.ekassir.ui.description.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.test.ekassir.EkassirApp;
import com.example.test.ekassir.R;
import com.example.test.ekassir.dagger.ActivityModule;
import com.example.test.ekassir.dagger.DaggerAppComponent;
import com.example.test.ekassir.data.model.Order;
import com.example.test.ekassir.ui.ActivityContract;
import com.example.test.ekassir.ui.description.presenter.DescriptionPresenter;
import com.example.test.ekassir.util.CurrencyHelper;
import com.example.test.ekassir.util.DateHelper;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DescriptionActivity extends AppCompatActivity implements ActivityContract.View {

    public static String orderDescription = "ORDER_DESCRIPTION";

    @Bind(R.id.toolbar) Toolbar mToolbar;
    @Bind(R.id.backdrop) ImageView mPhoto;
    @Bind(R.id.start_address) TextView mStartAddress;
    @Bind(R.id.end_address) TextView mEndAddress;
    @Bind(R.id.order_time) TextView mOrderTime;
    @Bind(R.id.order_price) TextView mPrice;
    @Bind(R.id.reg_number) TextView mRegNumber;
    @Bind(R.id.model_name) TextView mModelName;
    @Bind(R.id.divider_name) TextView mDividerName;

    @Inject
    DescriptionPresenter descriptionPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(null);
        }

        Order order = null;
        Intent intent = getIntent();
        if (intent != null) {
            order = (Order) intent.getSerializableExtra(orderDescription);
        }

        DaggerAppComponent.builder()
                .netComponent(((EkassirApp) getApplicationContext()).getNetComponent())
                .activityModule(new ActivityModule(this))
                .build().inject(this);

        mStartAddress.setText(order.getStartAddress().getAddress());
        mEndAddress.setText(order.getEndAddress().getAddress());
        mOrderTime.setText(DateHelper.convertFormat(order.getOrderTime()));
        mPrice.setText(
                CurrencyHelper.getAmountFormatted(
                        String.valueOf(order.getPrice().getAmount()),
                        order.getPrice().getCurrency()));
        mRegNumber.setText(order.getVehicle().getRegNumber());
        mModelName.setText(order.getVehicle().getModelName());
        mDividerName.setText(order.getVehicle().getDriverName());

        descriptionPresenter.loadPhoto(order.getVehicle().getPhoto());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public static void startDescriptionActivity(Context ctx, Order order) {
        Intent intent = new Intent(ctx, DescriptionActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(orderDescription, order);
        intent.putExtras(bundle);
        ctx.startActivity(intent);
    }

    @Override
    public void showOrders(List<Order> orders) {

    }

    @Override
    public void showPhoto(Bitmap bitmap) {
        mPhoto.setImageBitmap(bitmap);
    }

    @Override
    public void showError(String message) {
        //Show error message Toast
        Toast.makeText(getApplicationContext(), "Error" + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showComplete() {
        //Show completed message Toast
        Toast.makeText(getApplicationContext(), "Complete", Toast.LENGTH_SHORT).show();
    }
}
