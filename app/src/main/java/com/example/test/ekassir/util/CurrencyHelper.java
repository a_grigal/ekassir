package com.example.test.ekassir.util;

import android.support.annotation.NonNull;

/**
 * Created by Alexander Grigal on 06.02.2017
 */

public class CurrencyHelper {

    public static String getAmountFormatted(@NonNull String amount,
                             @NonNull String currency) {
        return new StringBuffer(amount)
                .insert(amount.length()-2, ".")
                .append(" ")
                .append(currency)
                .toString();
    }
}
