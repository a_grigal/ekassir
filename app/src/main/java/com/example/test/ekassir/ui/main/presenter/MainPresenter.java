package com.example.test.ekassir.ui.main.presenter;

import android.content.Context;

import com.example.test.ekassir.data.RestService;
import com.example.test.ekassir.data.model.Order;
import com.example.test.ekassir.ui.ActivityContract;
import com.example.test.ekassir.ui.description.view.DescriptionActivity;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import retrofit2.Retrofit;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Alexander Grigal on 05.02.2017
 */
public class MainPresenter implements ActivityContract.MainPresenter {

    public Retrofit retrofit;
    ActivityContract.View mView;

    @Inject
    public MainPresenter(Retrofit retrofit, ActivityContract.View mView) {
        this.retrofit = retrofit;
        this.mView = mView;
    }

    @Override
    public void loadOrders() {
        retrofit.create(RestService.class).getOrderList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Order>>() {
                    @Override
                    public void onCompleted() {
                        mView.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(List<Order> orders) {
                        Collections.sort(orders);
                        mView.showOrders(orders);
                    }
                });
    }

    @Override
    public void openDescription(Context ctx, Order order) {
        DescriptionActivity.startDescriptionActivity(ctx, order);
    }

}
