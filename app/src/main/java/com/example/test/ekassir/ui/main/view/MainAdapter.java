package com.example.test.ekassir.ui.main.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.test.ekassir.R;
import com.example.test.ekassir.data.model.Order;
import com.example.test.ekassir.data.model.Price;
import com.example.test.ekassir.ui.ActivityContract;
import com.example.test.ekassir.util.CurrencyHelper;
import com.example.test.ekassir.util.DateHelper;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Alexander Grigal on 05.02.2017
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private final Context mContext;
    private List<Order> mOrders;
    private ActivityContract.ItemClickListener mClickListener;
    
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.start_address) TextView mStartAddress;
        @Bind(R.id.end_address) TextView mEndAddress;
        @Bind(R.id.order_time) TextView mOrderTime;
        @Bind(R.id.order_price) TextView mPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public MainAdapter(Context ctx, List<Order> orders, ActivityContract.ItemClickListener clickListener) {
        mContext = ctx;
        mOrders = orders;
        mClickListener = clickListener;
    }

    @Override
    public MainAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final Order order = mOrders.get(position);
        Price price = order.getPrice();

        holder.mStartAddress.setText(String.format(mContext.getString(R.string.start_address_s),
                order.getStartAddress().getAddress()));
        holder.mEndAddress.setText(String.format(mContext.getString(R.string.end_address_s),
                order.getEndAddress().getAddress()));
        holder.mOrderTime.setText(String.format(mContext.getString(R.string.order_time_s),
                DateHelper.convertFormat(order.getOrderTime())));
        holder.mPrice.setText(String.format(mContext.getString(R.string.price_s),
                CurrencyHelper.getAmountFormatted(
                        String.valueOf(price.getAmount()),
                        price.getCurrency()
                )));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "onClick", Toast.LENGTH_LONG).show();
                mClickListener.onClick(order);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }
}

