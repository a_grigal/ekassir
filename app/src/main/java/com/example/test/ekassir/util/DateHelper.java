package com.example.test.ekassir.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Alexander Grigal on 06.02.2017
 */

public class DateHelper {

    private static final String TAG = "DateHelper";

    public static long getMillisecondsSinceEpoch(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZZZZ");
        Date gmt = null;
        try {
            gmt = formatter.parse(date);
        } catch (ParseException e) {
            Log.e(TAG, "getMillisecondsSinceEpoch: ", e.getCause());
        }

        return gmt.getTime();
    }

    public static String convertFormat(String date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        String formatted = format.format(getMillisecondsSinceEpoch(date));
        return formatted;
    }

}
