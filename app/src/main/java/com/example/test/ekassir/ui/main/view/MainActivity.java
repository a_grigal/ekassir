package com.example.test.ekassir.ui.main.view;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.core.deps.guava.annotations.VisibleForTesting;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.example.test.ekassir.EkassirApp;
import com.example.test.ekassir.R;
import com.example.test.ekassir.dagger.ActivityModule;
import com.example.test.ekassir.dagger.DaggerAppComponent;
import com.example.test.ekassir.data.model.Order;
import com.example.test.ekassir.ui.main.presenter.MainPresenter;
import com.example.test.ekassir.ui.ActivityContract;
import com.example.test.ekassir.util.EspressoIdlingResource;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity
        implements ActivityContract.View,
        ActivityContract.ItemClickListener {

    @Bind(R.id.toolbar) Toolbar mToolbar;
    @Bind(R.id.recycler_view) RecyclerView mRecyclerView;

    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;

    @Inject
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        mRecyclerView.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        DaggerAppComponent.builder()
                .netComponent(((EkassirApp) getApplicationContext()).getNetComponent())
                .activityModule(new ActivityModule(this))
                .build().inject(this);


        //Increment the counter before making a network request
        EspressoIdlingResource.increment();

        //Call the method in MainPresenter to make Network Request
        mainPresenter.loadOrders();
    }

    @Override
    public void showOrders(List<Order> orders) {
        mAdapter = new MainAdapter(this, orders, this);
        mRecyclerView.setAdapter(mAdapter);
        EspressoIdlingResource.decrement();
    }

    @Override
    public void showPhoto(Bitmap bitmap) {

    }

    @Override
    public void showError(String message) {
        //Show error message Toast
        Toast.makeText(getApplicationContext(), "Error" + message, Toast.LENGTH_SHORT).show();

        // If there is no network connection we get an error and decrement the counter because the call has finished
        EspressoIdlingResource.decrement();
    }

    @Override
    public void showComplete() {
        //Show completed message Toast
        Toast.makeText(getApplicationContext(), "Complete", Toast.LENGTH_SHORT).show();
    }

    @VisibleForTesting
    public IdlingResource getCountingIdlingResource() {
        return EspressoIdlingResource.getIdlingResource();
    }

    @Override
    public void onClick(Order order) {
        mainPresenter.openDescription(this, order);
    }

}
